package com.schedule.zyjshopping.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.schedule.zyjshopping.entity.Cation;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author LiuHuan
 * @since 2021-12-09
 */
public interface ICationService extends IService<Cation> {

    // 根据分类父ID进行查询
    Cation selectByIdTree(String id);

    // 分类添加
    int insert(Cation cation);

    // 分类查询
    List<Cation> selectCation(Cation cation);

    // 根据分类ID进行查询
    Cation selectId(String id);

    // 根据商品ID进行修改
    int updateId(Cation cation);

    // 根据分类ID进行删除商品
    int deleteId(String id);
}
