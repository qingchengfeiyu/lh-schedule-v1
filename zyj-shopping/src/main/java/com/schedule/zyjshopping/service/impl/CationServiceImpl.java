package com.schedule.zyjshopping.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.schedule.zyjshopping.entity.Cation;
import com.schedule.zyjshopping.mapper.CationMapper;
import com.schedule.zyjshopping.service.ICationService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author LiuHuan
 * @since 2021-12-09
 */
@Service
public class CationServiceImpl extends ServiceImpl<CationMapper, Cation> implements ICationService {


    @Autowired
    private CationMapper cationMapper;

    // 根据分类父Id进行查询
    @Override
    public Cation selectByIdTree(String id) {
        return selectTree(cationMapper.selectById(id));
    }

//    tree渲染方法抽出来，方便其他方法调用    传入需要查询树的对象信息
    public Cation selectTree(Cation cation){
//        判断父级是否为null
        if(!StringUtils.isEmpty(cation.getPId())){
//            查询父级信息
            Cation pCation = cationMapper.selectById(cation.getPId());
            pCation.setParentCation(cation);
            return selectTree(pCation);
        }else{
            return cation;
        }
    }

    // 添加分类
    @Override
    public int insert(Cation cation) {
        int insert = cationMapper.insert(cation);
        return insert;
    }
    // 查询分类
    @Override
    public List<Cation> selectCation(Cation cation) {
        List<Cation> cations = cationMapper.selectList(Wrappers.query(cation));
        return cations;
    }
    // 根据Id查询分类
    @Override
    public Cation selectId(String id) {
        return cationMapper.selectById(id);
    }
    // 根据Id修改分类信息
    @Override
    public int updateId(Cation cation) {
        return cationMapper.updateById(cation);
    }
    // 根据Id删除分类信息
    @Override
    public int deleteId(String id) {
        return cationMapper.deleteById(id);
    }
}
