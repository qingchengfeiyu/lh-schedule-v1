package com.schedule.zyjshopping.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.schedule.zyjshopping.entity.Product;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author LiuHuan
 * @since 2021-12-05
 */
public interface IProductService extends IService<Product> {


    // 商品添加
    int insert(Product product);

    // 商品查询
    List<Product> selectProduct(Product product);

//    查询商品附加分类信息
    List<Product> selectProductAndCation(Product product);

    // 根据商品ID进行查询
    Product selectId(String id);

    // 根据商品ID进行修改
    int updateId(Product product);

    // 根据商品ID进行删除商品
    int deleteId(String id);

    // 根据商品ID查询商品及其分类信息
    Product selectByIdCationTree(String id);
}
