package com.schedule.zyjshopping.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.schedule.zyjshopping.entity.Product;
import com.schedule.zyjshopping.mapper.ProductMapper;
import com.schedule.zyjshopping.service.ICationService;
import com.schedule.zyjshopping.service.IProductService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author LiuHuan
 * @since 2021-12-05
 */

@Service
public class ProductServiceImpl extends ServiceImpl<ProductMapper, Product> implements IProductService {

    @Autowired
    private ProductMapper productMapper;

    @Autowired
    private ICationService icationService;

    // 商品添加
    @Override
    public int insert(Product product) {

        int insert = productMapper.insert(product);
        return insert;

    }

    // 查询所有商品
    @Override
    public List<Product> selectProduct(Product product) {
        List<Product> selectProduct = productMapper.selectList(Wrappers.query(product));
        return selectProduct;
    }

    // 查询商品附加分类信息
    @Override
    public List<Product> selectProductAndCation(Product product) {
//        List<Product> selectProduct = new ArrayList<>();
//
////        遍历查询出的数据一个一个塞入分类信息   速度慢
//        productMapper.selectList(Wrappers.query(product)).forEach(one->{
//
//            if(!StringUtils.isEmpty(one.getCationId())){
//                one.setCation(icationService.selectByIdTree(one.getCationId()));
//            }
//            selectProduct.add(one);
//        });

        //        遍历查询出的数据多线程处理塞入分类信息   速度快
        List<Product> collect = productMapper.selectList(Wrappers.query(product)).parallelStream().map(one -> {
            if (!StringUtils.isEmpty(one.getCationId())) {
                one.setCation(icationService.selectByIdTree(one.getCationId()));
            }
            return one;
        }).collect(Collectors.toList());

        return collect;
    }

    // 根据ID进行查询商品ID
    @Override
    public Product selectId(String id) {
        return  productMapper.selectById(id);
    }

    // 根据商品ID进行修改商品内容
    @Override
    public int updateId(Product product) {
        return productMapper.updateById(product);
    }

    // 根据商品ID进行删除商品
    @Override
    public int deleteId(String id) {
        return productMapper.deleteById(id);
    }

    // 根据商品ID查询商品及其分类信息
    @Override
    public Product selectByIdCationTree(String id) {
        Product product = productMapper.selectById(id);
        product.setCation(icationService.selectByIdTree(product.getCationId()));
        return product;
    }
}
