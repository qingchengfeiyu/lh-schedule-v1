package com.schedule.zyjshopping;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author LiuHuan
 */
@SpringBootApplication(scanBasePackages = {"com.schedule"})
public class ZyjShoppingApplication {

    public static void main(String[] args) {
        SpringApplication.run(ZyjShoppingApplication.class, args);
    }
}
