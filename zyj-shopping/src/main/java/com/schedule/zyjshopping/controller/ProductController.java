package com.schedule.zyjshopping.controller;

import com.schedule.lhbasis.framework.dictFile.annotation.DictTranslation;
import com.schedule.lhbasis.framework.result.Result;
import com.schedule.zyjshopping.entity.Product;
import com.schedule.zyjshopping.service.IProductService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author LiuHuan
 * @since 2021-12-05
 */
@RestController
@RequestMapping("/shopping/product")
@Slf4j
@Validated
@Api("商品模块")
public class ProductController {

    @Autowired
    private IProductService iProductService;

    /**
     * 添加商品
     * @param product
     * @return
     */
    @PostMapping("insertProduct")
    @ApiOperation("添加商品")
    @ApiImplicitParams({@ApiImplicitParam(name = "id", value = "商品id",required = true,dataTypeClass = Product.class)})
    public Result insertProduct(Product product) {
        return Result.success(iProductService.insert(product));//这个就是封装返回数据库，iProductService.insert(product)就是要执行的方法
    }

    // 查询商品
    @PostMapping("selectProduct")
    @ApiOperation("查询商品")
    public Result selectProduct(Product product) {
        return Result.success(iProductService.selectProduct(product));
    }

    // 查询商品附加分类信息
    @PostMapping("selectProductAndCation")
    @ApiOperation(value = "查询商品及其附加分类信息",notes = "SELECT")
    public Result selectProductAndCation(Product product) {
        int i=1/0;
        return Result.success(iProductService.selectProductAndCation(product));
    }


    // 根据商品ID进行查询
    @PostMapping("selectId")
    @ApiOperation("根据id查询商品")
    @ApiImplicitParams({@ApiImplicitParam(name = "id", value = "商品id",required = true)})
    public Result selectId(@NotNull(message = "id不能为null") String id) {
        return Result.success(iProductService.selectId(id));
    }

    // 根据商品ID进行查询
    @PostMapping("selectIdOne")
    @ApiOperation(value = "根据商品ID进行查询",notes = "SELECT")
    @ApiImplicitParams({@ApiImplicitParam(name = "id", value = "商品id",required = true)})
    public Product selectIdOne(String id) {
        return iProductService.selectId(id);
    }

    // 根据商品ID进行查询
    @PostMapping("selectIdOneDict")
    @DictTranslation
    @ApiOperation(value = "根据商品ID进行查询字典翻译",notes = "SELECT")
    @ApiImplicitParams({@ApiImplicitParam(name = "id", value = "商品id",required = true)})
    public Product selectIdOneDict(String id) {
        return iProductService.selectId(id);
    }

    // 根据商品ID进行查询
    @PostMapping("selectIdOneDict2")
//    @DictTranslation
    @ApiOperation(value = "根据商品ID进行查询",notes = "SELECT")
    @ApiImplicitParams({@ApiImplicitParam(name = "id", value = "商品id",required = true)})
    public Result selectIdOneDict2(String id) {
        return Result.success(iProductService.selectId(id));
    }

    // 根据商品ID进行修改商品
    @PostMapping("updateProduct")
    @ApiOperation(value = "根据商品ID进行修改商品",notes = "UPDATE")
    @ApiImplicitParams({@ApiImplicitParam(name = "id", value = "商品id",required = true)})
    public Result updateProduct(Product product) {
        return Result.success(iProductService.updateId(product));
    }

    // 根据商品ID进行删除商品
    @PostMapping("deleteId")
    @ApiOperation(value = "根据商品ID进行删除商品",notes = "DELECT")
    @ApiImplicitParams({@ApiImplicitParam(name = "id", value = "商品id",required = true)})
    public Result deleteId(String id) {
        return Result.success(iProductService.deleteId(id));
    }

    // 根据商品ID查询商品及其分类信息
    @PostMapping("selectByIdCationTree")
    @ApiOperation("根据商品ID查询商品及其分类信息")
    @ApiImplicitParams({@ApiImplicitParam(name = "id", value = "商品id",required = true)})
    public Result selectByIdCationTree(String id) {
        return Result.success(iProductService.selectByIdCationTree(id));
    }


}
