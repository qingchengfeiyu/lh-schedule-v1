package com.schedule.zyjshopping.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;

import com.schedule.lhbasis.common.BaseEntity;
import com.schedule.lhbasis.framework.dictFile.annotation.Dict;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author LiuHuan
 * @since 2021-12-05
 */
@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
//@EqualsAndHashCode(callSuper = false)
@TableName("shopping_product")
public class Product extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableField("CATION_ID")
    private String cationId;

    @TableField("RESERVED2")
    private String reserved2;

    @TableField("RESERVED3")
    private String reserved3;

    @TableField("RESERVED4")
    private String reserved4;

    @TableField("RESERVED5")
    private String reserved5;

    /**
     * 商品名称
     */
    @TableField("TRADE_NAME")
    private String tradeName;

    /**
     * 商品价格
     */
    @TableField("TRADE_PRICE")
    private Double tradePrice;

    /**
     * 商品描述
     */
    @TableField("TRADE_DESC")
    private String tradeDesc;

    /**
     * 商品状态
     */
    @Dict(dictName = "PRODUCT_STATUS",nullValueName = "我是奥里给",undefinedValue = "啊啊啊啊啊啊啊")
    @TableField("TRADE_STATUS")
    private String tradeStatus;

    @TableField(exist = false)
    private String tradeStatusName;

    /**
     * 商品点击量
     */
    @TableField("TRADE_PV")
    private Integer tradePv;

    @TableField(exist = false)
    private Cation cation;

}
