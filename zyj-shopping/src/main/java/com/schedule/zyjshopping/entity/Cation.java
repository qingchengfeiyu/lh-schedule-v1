package com.schedule.zyjshopping.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.schedule.lhbasis.common.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author LiuHuan
 * @since 2021-12-09
 */
@Data
//@EqualsAndHashCode(callSuper = false)
@TableName("shopping_cation")
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class Cation extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableField("RESERVED1")
    private String reserved1;

    @TableField("RESERVED2")
    private String reserved2;

    @TableField("RESERVED3")
    private String reserved3;

    @TableField("RESERVED4")
    private String reserved4;

    @TableField("RESERVED5")
    private String reserved5;

    /**
     * 父分类id
     */
    @TableField("P_ID")
    private String pId;

    /**
     * 分类名称
     */
    @TableField("CATION_NAME")
    private String cationName;

    /**
     * 分类描述
     */
    @TableField("CATION_DESCRIBE")
    private String cationDescribe;

//    父级信息
    @TableField(exist = false)
    private Cation parentCation;

}
