package com.schedule.zyjshopping.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.schedule.zyjshopping.entity.Cation;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author LiuHuan
 * @since 2021-12-09
 */
@Mapper
public interface CationMapper extends BaseMapper<Cation> {

}
