package com.schedule.zyjshopping.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.schedule.zyjshopping.entity.Product;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author LiuHuan
 * @since 2021-12-05
 */
@Mapper
public interface ProductMapper extends BaseMapper<Product> {

}
