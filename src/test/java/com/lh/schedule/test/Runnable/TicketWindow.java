package com.lh.schedule.test.Runnable;

import java.util.concurrent.locks.ReentrantLock;

public class TicketWindow implements Runnable{

    private  int ticket=100;

    private ReentrantLock lock = new ReentrantLock();

    public synchronized void sellTickets(){
        if(ticket>0){
            ticket--;
            System.out.println(Thread.currentThread().getName()+"-售票剩余:"+ticket);
        }
    }

    @Override
    public void run() {
        while (ticket>0) {
                sellTickets();
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
        }
        System.out.println(Thread.currentThread().getName()+"-票不足了:"+ticket);

    }
}
