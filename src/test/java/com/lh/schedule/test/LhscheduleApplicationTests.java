package com.lh.schedule.test;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import com.lh.schedule.businessFile.basis.controller.SysDictController;
import com.lh.schedule.businessFile.basis.entity.SysDict;
import com.lh.schedule.businessFile.basis.entity.SysUser;
import com.lh.schedule.businessFile.basis.service.ISysDictService;
import com.lh.schedule.businessFile.basis.service.ISysLogService;
import com.lh.schedule.businessFile.basis.service.ISysUserService;
import com.lh.schedule.businessFile.brief.server.IStatusAnalyseService;
import com.lh.schedule.businessFile.shopping.entity.Cation;
import com.lh.schedule.businessFile.shopping.entity.Product;
import com.lh.schedule.businessFile.shopping.mapper.ProductMapper;
import com.lh.schedule.businessFile.shopping.service.IProductService;
import com.lh.schedule.businessFile.utli.RedisUtils;
import com.lh.schedule.businessFile.utli.SystemTools;
import com.lh.schedule.test.Runnable.TicketWindow;
import lombok.extern.slf4j.Slf4j;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.security.core.parameters.P;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;

@SpringBootTest
@Slf4j
class LhscheduleApplicationTests {

    @Autowired
    private ISysLogService iSysLogService;

    @Autowired
    private ISysDictService iSysDictService;

    @Autowired
    private ISysUserService iSysUserService;

    @Autowired
    SysDictController sysDictController;

    @Autowired
    private ProductMapper productMapper;




//    @Test
//    void contextLoads() {
//        System.err.println(sysDictController.test2("3"));
//    }


    @Test
    void contextLoads2() {
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        HttpServletRequest httpServletRequest= (HttpServletRequest) requestAttributes.resolveReference(RequestAttributes.REFERENCE_REQUEST);
        System.err.println(SystemTools.getIPAddress(httpServletRequest));
    }

    @Test
    void contextLoads3() {
        System.out.println("\033[91;44m" +"啊啊啊啊啊啊啊啊啊啊啊啊啊啊"+"\033[m");
        System.out.println("\033[92;44m" +"啊啊啊啊啊啊啊啊啊啊啊啊啊啊"+"\033[m");
        System.out.println("\033[93;44m" +"啊啊啊啊啊啊啊啊啊啊啊啊啊啊"+"\033[m");
        System.out.println("\033[94;44m" +"啊啊啊啊啊啊啊啊啊啊啊啊啊啊"+"\033[m");
        System.out.println("\033[95;44m" +"啊啊啊啊啊啊啊啊啊啊啊啊啊啊"+"\033[m");
        System.out.println("\033[96;44m" +"啊啊啊啊啊啊啊啊啊啊啊啊啊啊"+"\033[m");
        System.out.println("\033[97;44m" +"啊啊啊啊啊啊啊啊啊啊啊啊啊啊"+"\033[m");

        System.out.println("============================================");

        System.out.println("\033[40m" +"啊啊啊啊啊啊啊啊啊啊啊啊啊啊"+"\033[m");
        System.out.println("\033[41m" +"啊啊啊啊啊啊啊啊啊啊啊啊啊啊"+"\033[m");
        System.out.println("\033[42m" +"啊啊啊啊啊啊啊啊啊啊啊啊啊啊"+"\033[m");
        System.out.println("\033[43m" +"啊啊啊啊啊啊啊啊啊啊啊啊啊啊"+"\033[m");
        System.out.println("\033[44m" +"啊啊啊啊啊啊啊啊啊啊啊啊啊啊"+"\033[m");
        System.out.println("\033[45m" +"啊啊啊啊啊啊啊啊啊啊啊啊啊啊"+"\033[m");
        System.out.println("\033[46m" +"啊啊啊啊啊啊啊啊啊啊啊啊啊啊"+"\033[m");
        System.out.println("\033[47m" +"啊啊啊啊啊啊啊啊啊啊啊啊啊啊"+"\033[m");
        System.out.println("============================================");
        System.out.println("\033[93;41m" +"啊啊啊啊啊啊啊啊啊啊啊啊啊啊"+"\033[m");
        System.out.println("\033[97;41m" +"啊啊啊啊啊啊啊啊啊啊啊啊啊啊"+"\033[m");

        System.out.println("============================================");

        System.out.println("\033[33m" +"啊啊啊啊啊啊啊啊啊啊啊啊啊啊"+"\033[m");


    }

//    @Test
//    @DictTranslation
//    void test2(){
//        SysUser sysUser = new SysUser();
//        sysUser.setSex("1");
//
//        System.err.println("翻译："+sysUser);
//    }

    @Test
    void test3(){
        System.err.println(iSysDictService.redisTest(11).toString());
        System.err.println(iSysDictService.redisTest(12).toString());
        System.err.println(iSysDictService.redisTest(12).toString());
        System.err.println(iSysDictService.redisTest(11).toString());
    }

    @Autowired
    private RedisUtils redisUtils;

    @Test
    void encryption(){

        redisUtils.set("sysDictList", JSONObject.toJSONString(iSysDictService.list()));

        List<SysDict> sysDictList = JSONArray.parseArray(redisUtils.get("sysDictList").toString(),SysDict.class);
    }



    @Test
    void test4(){
        SysUser applicant=new SysUser();
        applicant.setId("1");

        SysUser otherParty=new SysUser();
        otherParty.setId("2");

        iSysUserService.replaceTheName(applicant,otherParty);

        System.err.println("完成");
    }


    @Autowired
    private List<IStatusAnalyseService> statusAnalyseServiceList;


    @Test
    void sayHelloAsync() {
        for (IStatusAnalyseService statusAnalyseService : statusAnalyseServiceList) {
//            statusAnalyseService.doStatusAnalyseHandle(null, null);
            statusAnalyseService.doStatusAnalyseHandle(null);
        }
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Qualifier("mo")
//    @Autowired
    private IStatusAnalyseService iStatusAnalyseService;

    @Test
    void tetsHelloAsysncTow(){

        for (int i = 1; i <10;i++){
            iStatusAnalyseService.ticket(String.valueOf(i));
        }

        System.out.println("处理完成");
    }

//    @Test
//    void test(){
////        List<SysUser> list = iSysUserService.list();
////
////        System.err.println(list);
//        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
//        System.err.println(bCryptPasswordEncoder.encode("123123"));
//    }


    // 测试增加商品
    @Test
    public void insertProduct(){

        Product product = new Product();
        product.setTradeName("红米K20pro至尊版");  // 商品名称
        product.setTradePrice(3499.0);      // 商品价格
        product.setTradeDesc("小米手机性价比很高，性能杠杠的。");  // 商品描述
        product.setTradeStatus("0");   // 商品状态 0：上架  1：下架
        product.setTradePv(1000000);   // 商品点击量
        productMapper.insert(product);
        System.out.println(product);
    }
    // 测试查询所有商品记录
    @Test
    public void selectProduct() {
        List<Product> selectProducts = productMapper.selectList(null);
        selectProducts.forEach(System.out::println);
//        System.out.println(selectProducts);
    }

    // 测试根据商品id查询商品记录
    @Test
    public void selectProductById() {
        String id="07f7c58e93ba2884e2e73f6a8f3c2e03";
        Product selectProducts = productMapper.selectById(id);
        System.err.println(selectProducts);
//        System.out.println(selectProducts);
    }
    // 测试根据商品id修改商品信息
    @Test
    public void updateProduct(){
        // 实例化一个商品实体对象
        Product product = new Product();
        // 根据ID查询一定要设置ID 　要不然没ID拿jb查　所以很重要
        product.setId("07f7c58e93ba2884e2e73f6a8f3c2e03");
        // 接下来给商品修改内容
        product.setTradeName("...");
        // 然后调用Mapper中的updateId方法进行修改 并且传入上面设置的参数
        int i = productMapper.updateById(product);
        // 输出修改后的结果
        System.out.println(i);
    }
    // 测试根据商品id删除商品信息
    @Test
    public void deleteId(){
        // 首先实例化商品实体对象
//        Product product = new Product();
        // 根据ID删除商品一定要设置商品ID 否则不能删除
//        product.setId("07f7c58e93ba2884e2e73f6a8f3c2e03");
        // 然后是用该商品实体类对应的Mapper调用恩据id删除的方法进行删除内容
        int i = productMapper.deleteById("07f7c58e93ba2884e2e73f6a8f3c2e03");
        // 输出结果
        System.out.println(i);

    }



    @Test
    void testTicket(){
        TicketWindow ticketWindow=new TicketWindow();

        for (int i = 0; i < 8;i++) {
            Thread t=new Thread(ticketWindow,"窗口:"+i);
            t.start();
        }

        System.err.println("进行其他操作");
        try {
            Thread.sleep(50000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


//    newCachedThreadPool()
    @Test
    void threadPoll1() throws Exception{
        long startTime=System.currentTimeMillis();
        ExecutorService executor = Executors.newSingleThreadExecutor();
        ExecutorService executor2 = Executors.newSingleThreadExecutor();
        ExecutorService executor3 = Executors.newSingleThreadExecutor();

        System.err.println("机器打开");

        Future<String> future1 = executor.submit(() -> {
            System.out.println("开始处理零件a");
            Thread.sleep(2000);
            return "a.id";
        });

        Future<String> future2 = executor2.submit(() -> {
            System.out.println("开始处理零件b");
            Thread.sleep(2000);
            return "b.id";
        });

        Future<String> future3 = executor3.submit(() -> {
            System.out.println("开始处理零件c");
            Thread.sleep(2000);
            return "c.id";
        });

//        executor.shutdown();
//        executor2.shutdown();
//        executor3.shutdown();
        // 这里会阻塞直到future.get返回值或者超时
        System.out.println("组装零件"+future1.get()+","+future2.get()+","+future3.get());

        long endTime=System.currentTimeMillis();

        System.out.println("当前程序耗时："+(endTime-startTime)+"ms");
    }

    @Test
    void threadPoll2() throws Exception{
        long startTime=System.currentTimeMillis();

        Future<String> future = Executors.newSingleThreadExecutor().submit(() -> {
            System.out.println("开始处理零件a");
            Thread.sleep(2000);
            return "a.id";
        });

        System.out.println("获取:"+future.get());

        long endTime=System.currentTimeMillis();

        System.out.println("当前程序耗时："+(endTime-startTime)+"ms");
    }

    @Test
    void threadPoll3() throws Exception{
        CompletableFuture.supplyAsync(()->callAPI("param","a"))
                .thenApply(res->callAPI(res,"b"))
                .thenApply(res->callAPI(res,"c"))
                .thenAccept(System.out::println);
        // 等异步任务输出
        Thread.sleep(20000);
    }

    String callAPI(String param, String mockRes) {
        // 模拟api调用，省略try-catch
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return mockRes;
    }

    @Test
    void testSys(){
        Map<String, String> map = System.getenv();
        String userName = map.get("USERNAME");// 获取用户名
        String computerName = map.get("COMPUTERNAME");// 获取计算机名
        String userDomain = map.get("USERDOMAIN");// 获取计算机域名
        System.out.println("userName:"+userName);
        System.out.println("computerName:"+computerName);
        System.out.println("userDomain:"+userDomain);
    }

//    获取分类树
    @Test
    void test5(){
        List<Cation> list=new ArrayList<>();
        list.add(Cation.builder().id("10").cationName("苹果手机").pId("1").build());
        list.add(Cation.builder().id("11").cationName("华为手机").pId("1").build());
        list.add(Cation.builder().id("20").cationName("华硕电脑").pId("2").build());
        list.add(Cation.builder().id("30").cationName("苹果11pro").pId("10").build());
        list.add(Cation.builder().id("21").cationName("手机").build());
        list.add(Cation.builder().id("21").cationName("电脑").build());
    }

}
