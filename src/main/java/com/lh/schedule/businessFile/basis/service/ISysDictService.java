package com.lh.schedule.businessFile.basis.service;

import com.lh.schedule.businessFile.basis.entity.SysDict;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author liuhuan
 * @since 2021-11-19
 */
public interface ISysDictService extends IService<SysDict> {

    List<SysDict> redisTest(Integer t);
}
