package com.lh.schedule.businessFile.basis.entity;

import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

import com.lh.schedule.businessFile.common.BaseEntity;
import lombok.*;
import lombok.experimental.SuperBuilder;

/**
 * <p>
 * 
 * </p>
 *
 * @author LiuHuan
 * @since 2021-12-06
 */
@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class SysRole  extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableField("RESERVED1")
    private String reserved1;

    @TableField("RESERVED2")
    private String reserved2;

    @TableField("RESERVED3")
    private String reserved3;

    @TableField("RESERVED4")
    private String reserved4;

    @TableField("RESERVED5")
    private String reserved5;

    /**
     * 名称
     */
    @TableField("ROLE_NAME")
    private String roleName;

    /**
     * 类型
     */
    @TableField("ROLE_TYPE")
    private String roleType;


}
