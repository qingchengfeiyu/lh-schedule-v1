package com.lh.schedule.businessFile.basis.controller;

import com.lh.schedule.businessFile.basis.entity.SysDict;
import com.lh.schedule.businessFile.basis.service.ISysDictService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PostMapping;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author kenx
 * @since 2021-11-19
 */
@RestController
@RequestMapping("/basis/sysDict")
@Api("系统字典模块")
public class SysDictController {

    @Autowired
    ISysDictService iSysDictService;


    @PostMapping("add")
    public boolean add(SysDict sysDict){
        return iSysDictService.save(sysDict);
    }
}
