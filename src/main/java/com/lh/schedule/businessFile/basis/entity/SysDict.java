package com.lh.schedule.businessFile.basis.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

import com.lh.schedule.businessFile.common.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 * <p>
 * 
 * </p>
 *
 * @author liuhuan
 * @since 2021-11-19
 */
@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class SysDict extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 字典类型
     */
    @TableField("TYPE_CODE")
    private String typeCode;

    /**
     * 展示值
     */

    @TableField("NAME")
    private String name;

    /**
     * 实际值
     */
    @TableField("VALUE")
    private String value;

    /**
     * 排序
     */
    @TableField("SORT")
    private String sort;

    /**
     * 父类id
     */
    @TableField("SUPER_ID")
    private String superId;



}
