package com.lh.schedule.businessFile.basis.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lh.schedule.businessFile.basis.entity.SysErrorLogEntity;

public interface IErrorLogService extends IService<SysErrorLogEntity> {
}
