package com.lh.schedule.businessFile.basis.service;

//import com.lh.schedule.businessFile.basis.entity.SysUser;
import com.baomidou.mybatisplus.extension.service.IService;
import com.lh.schedule.businessFile.basis.entity.SysUser;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author LiuHuan
 * @since 2021-12-03
 */
public interface ISysUserService extends IService<SysUser> {

    boolean replaceTheName(SysUser applicant,SysUser otherParty);

    List<SysUser> listSysUser();
}
