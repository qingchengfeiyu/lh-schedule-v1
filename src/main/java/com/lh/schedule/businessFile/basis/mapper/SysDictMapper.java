package com.lh.schedule.businessFile.basis.mapper;

import com.lh.schedule.businessFile.basis.entity.SysDict;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author kenx
 * @since 2021-11-19
 */
@Mapper
public interface SysDictMapper extends BaseMapper<SysDict> {

}
