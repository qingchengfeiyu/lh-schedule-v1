package com.lh.schedule.businessFile.basis.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lh.schedule.businessFile.basis.entity.SysLogEntity;

public interface SysLogMapper extends BaseMapper<SysLogEntity> {
}
