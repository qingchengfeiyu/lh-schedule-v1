package com.lh.schedule.businessFile.basis.service;

import com.lh.schedule.businessFile.basis.entity.SysRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author LiuHuan
 * @since 2021-12-06
 */
public interface ISysRoleService extends IService<SysRole> {

}
