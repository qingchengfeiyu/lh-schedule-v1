package com.lh.schedule.businessFile.basis.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.lh.schedule.businessFile.common.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@TableName("sys_log_info")
public class SysLogEntity extends BaseEntity implements Serializable{

//    类名
    @TableField("CLASS_NAME")
    private String className;

//    方法名
    @TableField("METHOD_NAME")
    private String methodName;

//    请求参数
    @TableField("PARAMS")
    private String params;

//    请求类型
    @TableField("REQUEST_TYPE")
    private String requestType;

//    一级目录
    @TableField("ONE_LEVEL")
    private String oneLevel;

//    二级目录
    @TableField("TOW_LEVEL")
    private String towLevel;

//    返回结果
    @TableField("RESULTS")
    private String results;

//    用时
    @TableField("WHEN_TIME")
    private String whenTime;
}
