package com.lh.schedule.businessFile.basis.service.impl;

import com.lh.schedule.businessFile.basis.entity.SysDict;
import com.lh.schedule.businessFile.basis.mapper.SysDictMapper;
import com.lh.schedule.businessFile.basis.service.ISysDictService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author kenx
 * @since 2021-11-19
 */
@Service
public class SysDictServiceImpl extends ServiceImpl<SysDictMapper, SysDict> implements ISysDictService {

    @Autowired
    ISysDictService iSysDictService;

    @Override
    //表示使用my-redis-cache1缓存空间，key的生成策略为redis+t，当t>10的时候才会使用缓存
    @Cacheable(value = "my-redis-cache1")
    public List<SysDict> redisTest(Integer t) {
        return iSysDictService.list();
    }
}
