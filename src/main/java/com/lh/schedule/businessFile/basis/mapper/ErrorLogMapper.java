package com.lh.schedule.businessFile.basis.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lh.schedule.businessFile.basis.entity.SysErrorLogEntity;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ErrorLogMapper extends BaseMapper<SysErrorLogEntity> {
}
