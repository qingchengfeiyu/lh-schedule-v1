package com.lh.schedule.businessFile.basis.controller;


import io.swagger.annotations.Api;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author LiuHuan
 * @since 2021-12-06
 */
@RestController
@RequestMapping("/basis/sys-role")
@Api("系统角色模块")
public class SysRoleController {

}
