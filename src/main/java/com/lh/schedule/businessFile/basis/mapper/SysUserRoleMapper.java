package com.lh.schedule.businessFile.basis.mapper;

import com.lh.schedule.businessFile.basis.entity.SysUserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author LiuHuan
 * @since 2021-12-06
 */
public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {

}
