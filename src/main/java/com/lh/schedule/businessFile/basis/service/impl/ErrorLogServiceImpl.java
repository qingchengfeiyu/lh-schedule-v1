package com.lh.schedule.businessFile.basis.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lh.schedule.businessFile.basis.mapper.ErrorLogMapper;
import com.lh.schedule.businessFile.basis.entity.SysErrorLogEntity;
import com.lh.schedule.businessFile.basis.service.IErrorLogService;
import org.springframework.stereotype.Service;

@Service
public class ErrorLogServiceImpl  extends ServiceImpl<ErrorLogMapper, SysErrorLogEntity> implements IErrorLogService {
}
