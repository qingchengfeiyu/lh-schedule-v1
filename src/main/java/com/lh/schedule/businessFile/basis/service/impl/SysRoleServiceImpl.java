package com.lh.schedule.businessFile.basis.service.impl;

import com.lh.schedule.businessFile.basis.entity.SysRole;
import com.lh.schedule.businessFile.basis.mapper.SysRoleMapper;
import com.lh.schedule.businessFile.basis.service.ISysRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author LiuHuan
 * @since 2021-12-06
 */
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements ISysRoleService {

}
