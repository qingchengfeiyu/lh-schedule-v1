package com.lh.schedule.businessFile.basis.controller;


import com.lh.schedule.basisFile.annotations.SysLog;
import com.lh.schedule.basisFile.dictFile.annotation.DictTranslation;
import com.lh.schedule.basisFile.enums.OperationType;
import com.lh.schedule.basisFile.result.Result;
import com.lh.schedule.basisFile.result.Result;
import com.lh.schedule.businessFile.basis.entity.SysUser;
import com.lh.schedule.businessFile.basis.service.ISysUserService;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.apache.catalina.security.SecurityUtil;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.access.prepost.PreAuthorize;
//import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author LiuHuan
 * @since 2021-12-03
 */
@RestController
@RequestMapping("/basis/sysUser")
@Slf4j
@Api("系统用户模块")
public class SysUserController {

    @Autowired
    private ISysUserService isysUserService;


    @PostMapping("login")
    public boolean userLogin(SysUser sysUser){
        System.out.println("获取：");

        System.err.println(sysUser);
        return true;
    }

    @PostMapping("getById")
    public SysUser getUserById(String id){
        return isysUserService.getById(id);
    }


//    默认方式
    @PostMapping("listUser1")
    public List<SysUser> sysUserList1(){
        return isysUserService.list();
    }

//    加入log日志
    @PostMapping("listUser2")
    // @SysLog(oneLevel = "用户操作",towLevel = "用户查询全部2",requestType = OperationType.SELECT)
    public List<SysUser> sysUserList2(){
//        异常
        int a=1/0;
        return isysUserService.list();
    }

    //    加入log日志
    @PostMapping("listUser2error")
    // @SysLog(oneLevel = "用户操作",towLevel = "用户查询全部2error",requestType = OperationType.SELECT)
    public List<SysUser> sysUserList2error(){
//        异常
        int a=100/0;
        return isysUserService.list();
    }

//    封装返回值
    @PostMapping("listUser3")
    // @SysLog(oneLevel = "用户操作",towLevel = "用户查询全部3",requestType = OperationType.SELECT)
    public Result sysUserList3(){
        return Result.success(isysUserService.list());
    }


    //    原生写法
    @PostMapping("listUserDict1")
    // @SysLog(oneLevel = "用户操作",towLevel = "用户查询全部字典翻译处理1",requestType = OperationType.SELECT)
    public List<SysUser> sysUserListDict1(){
        return isysUserService.listSysUser();
    }

    //    封装全自动字典翻译   只需要在方法上加入一个注解   第二部在实体配置翻译信息
    @PostMapping("listUserDict2")
    // @SysLog(oneLevel = "用户操作",towLevel = "用户查询全部字典翻译处理2",requestType = OperationType.SELECT)
    @DictTranslation
    public List<SysUser> sysUserListDict2(){
        return isysUserService.list();
    }


    @PostMapping("/")
    public String showHome() {
        String name = "";
//        String name = SecurityContextHolder.getContext().getAuthentication().getName();
        log.info("当前登陆用户：" + name);

        return name;
    }


    @PostMapping("/admin")
    @ResponseBody
//    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public String printAdmin() {
        return "如果你看见这句话，说明你有ROLE_ADMIN角色";
    }

    @PostMapping("/user")
    @ResponseBody
//    @PreAuthorize("hasRole('ROLE_BASIS')")
    public String printUser() {
        return "如果你看见这句话，说明你有ROLE_USER角色";
    }
}
