package com.lh.schedule.businessFile.basis.service.impl;

import com.lh.schedule.businessFile.basis.entity.SysUserRole;
import com.lh.schedule.businessFile.basis.mapper.SysUserRoleMapper;
import com.lh.schedule.businessFile.basis.service.ISysUserRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author LiuHuan
 * @since 2021-12-06
 */
@Service
public class SysUserRoleServiceImpl extends ServiceImpl<SysUserRoleMapper, SysUserRole> implements ISysUserRoleService {

}
