package com.lh.schedule.businessFile.basis.mapper;

import com.lh.schedule.businessFile.basis.entity.SysUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author LiuHuan
 * @since 2021-12-03
 */
@Mapper
public interface SysUserMapper extends BaseMapper<SysUser> {

    List<SysUser> sysUserList();;
}
