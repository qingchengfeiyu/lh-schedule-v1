package com.lh.schedule.businessFile.basis.controller;


import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.PostMapping;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author LiuHuan
 * @since 2021-12-06
 */
@RestController
@RequestMapping("/basis/sys-user-role")
@Api("系统用户和角色关联模块")
public class SysUserRoleController {

}
