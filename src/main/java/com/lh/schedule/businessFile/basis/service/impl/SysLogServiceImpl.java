package com.lh.schedule.businessFile.basis.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lh.schedule.businessFile.basis.mapper.SysLogMapper;
import com.lh.schedule.businessFile.basis.entity.SysLogEntity;
import com.lh.schedule.businessFile.basis.service.ISysLogService;
import org.springframework.stereotype.Service;

@Service
public class SysLogServiceImpl extends ServiceImpl<SysLogMapper, SysLogEntity> implements ISysLogService {
}
