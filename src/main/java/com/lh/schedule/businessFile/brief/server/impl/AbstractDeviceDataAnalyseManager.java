package com.lh.schedule.businessFile.brief.server.impl;

import com.lh.schedule.businessFile.brief.server.IStatusAnalyseService;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.Random;

@Service("mo")
public abstract class AbstractDeviceDataAnalyseManager implements IStatusAnalyseService {



    @Async("customizeThreadPool")
    @Override
    public void doStatusAnalyseHandle(String start, String end) {
        int sleepSeconds = new Random().nextInt(3) + 1;
        try {
            Thread.sleep(sleepSeconds * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.err.println(getDataType() + "在自定义线程" + Thread.currentThread().getName() + "执行了" + sleepSeconds + "秒");
    }


    @Async
    @Override
    public void doStatusAnalyseHandle(String end) {
        int sleepSeconds = new Random().nextInt(3) + 1;
        try {
            Thread.sleep(sleepSeconds * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(getDataType() + "在默认线程" + Thread.currentThread().getName() + "执行了" + sleepSeconds + "秒");
    }

    @Override
    @Async
    public void ticket(String id) {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.err.println("售票处理中...."+id);
    }
}