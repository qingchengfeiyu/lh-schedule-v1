package com.lh.schedule.businessFile.brief.server.impl;

import org.springframework.stereotype.Service;

@Service
public class ZHUDeviceDataAnalyseManager extends AbstractDeviceDataAnalyseManager {

    @Override
    public String getDataType() {
        return "二号窗口";
    }

}