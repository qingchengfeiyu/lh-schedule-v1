package com.lh.schedule.businessFile.brief.server;

public interface IStatusAnalyseService {

    /**
     * 设备状态解析处理
     *
     * @param start 开始时间
     * @param end   截止时间
     */
    void doStatusAnalyseHandle(String start, String end);

    /**
     * 设备状态解析处理
     *
     * @param end 截止时间
     */
    void doStatusAnalyseHandle(String end);

    /**
     * 获取数据类别
     *
     * @return
     */
    String getDataType();


    void ticket(String id);
}
