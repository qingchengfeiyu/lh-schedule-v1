package com.lh.schedule.businessFile.common;

import com.baomidou.mybatisplus.annotation.*;
import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@Data
@ToString(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class BaseEntity implements Serializable {

    @TableId(value = "ID",type = IdType.ASSIGN_UUID)
    @NotNull(message = "ID不能为NULL")
    private String id;

    //    发布时间
    @TableField(value = "CREATE_DATE",fill = FieldFill.INSERT)
    private Date createDate;

    //    创建人
    @TableField(value = "CREATE_USER",fill = FieldFill.INSERT)
    private String createUser;

    //    创建人ip
    @TableField(value = "CREATE_USER_IP",fill = FieldFill.INSERT)
    private String createUserIp;

    //    修改时间
    @TableField(value = "UPDATE_DATE",fill = FieldFill.UPDATE)
    private Date updateDate;

    //    修改人
    @TableField(value = "UPDATE_USER",fill = FieldFill.UPDATE)
    private String updateUser;

    //    修改人ip
    @TableField(value = "UPDATE_USER_IP",fill = FieldFill.UPDATE)
    private String updateUserIp;

    //    备注
    @TableField("REMARK")
    private String remark;

    @TableLogic
    @TableField(value = "LOGIC_DEL",fill = FieldFill.INSERT)
    private Integer logicDel;

    @Version
    @TableField(value = "VERSION",fill = FieldFill.INSERT)
    private Integer version;


}
