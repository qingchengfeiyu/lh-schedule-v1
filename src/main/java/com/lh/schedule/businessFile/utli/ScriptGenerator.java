package com.lh.schedule.businessFile.utli;

import com.baomidou.mybatisplus.core.exceptions.MybatisPlusException;
import io.micrometer.core.instrument.util.IOUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import java.io.*;
import java.net.URL;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

@Slf4j
public class ScriptGenerator {


    public static void main(String[] args) {
        System.err.println("请先更新至最新项目后在进行本操作,如果脚本未找到请刷新文件");

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        String type=scanner("你要进行init(表)添加还是doc(非表)添加");
        String name=scanner("模块名 例如:shopping;system..等");
        String operationType=scanner("操作类型 例如:add,update,del...等");
        String table=scanner("操作表名");
        String describe=scanner("描述");



        String path="db/migration/"+type+"/"+name+"/"+(new SimpleDateFormat("yyyy/MM/dd").format(new Date()));
        System.err.println("============================================================");
        System.err.println("生成结果:"+getListContent(path, operationType, table, describe));
        System.err.println("============================================================");

    }


    /**
     * <p>
     * 读取控制台内容
     * </p>
     */
    public static String scanner(String tip) {
        Scanner scanner = new Scanner(System.in);
        StringBuilder help = new StringBuilder();
        help.append("请输入" + tip + "：");
        System.out.println(help.toString());
        if (scanner.hasNext()) {
            String ipt = scanner.next();
            if (StringUtils.isNotBlank(ipt)) {
                return ipt;
            }
        }
        throw new MybatisPlusException("请输入正确的" + tip + "！");
    }


    public static String getListContent(String filePath,String operationType,String table,String describe) {


        File file =new File(System.getProperty("user.dir")+File.separator+ "src" + File.separator + "main" + File.separator + "resources"+File.separator +filePath);
//如果文件夹不存在则创建
        if  (!file .exists()  && !file .isDirectory()) {
            file.mkdir();
        }

        List<String> files = new ArrayList<String>();
        File[] tempList = file.listFiles();

        for (int i = 0; i < tempList.length; i++) {
            if (tempList[i].isFile()) {
//                files.add(tempList[i].toString());
                //文件名，不包含路径
                files.add(tempList[i].getName());
            }
        }
        System.out.println("今日脚本如下");

        String max="00";
        for (String re : files) {
            System.out.println(re);

            max=max.compareTo(re.split("__")[0].split("_")[1])>0?max:re.split("__")[0].split("_")[1];
        }

        String value="V"+(new SimpleDateFormat("yyyyMMdd").format(new Date()))+"_"+addOne(max)+"__"+operationType+"-"+table+"-"+describe+".sql";

        try {
            fileWriterMethod((file.getAbsolutePath()+"\\/")+value, "");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return value;
    }


    public static String addOne(String testStr) {
        String[] strs = testStr.split("[^0-9]");//根据不是数字的字符拆分字符串
        String numStr = strs[strs.length - 1];//取出最后一组数字
        if (numStr != null && numStr.length() > 0) {//如果最后一组没有数字(也就是不以数字结尾)，抛NumberFormatException异常
            int n = numStr.length();//取出字符串的长度
            int num = Integer.parseInt(numStr) + 1;//将该数字加一
            String added = String.valueOf(num);
            n = Math.min(n, added.length());
            //拼接字符串
            return testStr.subSequence(0, testStr.length() - n) + added;
        } else {
            throw new NumberFormatException();
        }
    }


    /**
     * 方法 1：使用 FileWriter 写文件
     * @param filepath 文件目录
     * @param content  待写入内容
     * @throws IOException
     */
    public static void fileWriterMethod(String filepath, String content) throws IOException {
        try (FileWriter fileWriter = new FileWriter(filepath)) {
            fileWriter.append(content);
        }
    }
}
