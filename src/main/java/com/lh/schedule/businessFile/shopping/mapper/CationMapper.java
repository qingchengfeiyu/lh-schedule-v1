package com.lh.schedule.businessFile.shopping.mapper;

import com.lh.schedule.businessFile.shopping.entity.Cation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author LiuHuan
 * @since 2021-12-09
 */
@Mapper
public interface CationMapper extends BaseMapper<Cation> {

}
