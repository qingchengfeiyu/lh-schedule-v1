package com.lh.schedule.businessFile.shopping.controller;


import com.lh.schedule.basisFile.annotations.SysLog;
import com.lh.schedule.basisFile.dictFile.annotation.DictTranslation;
import com.lh.schedule.basisFile.enums.OperationType;
import com.lh.schedule.basisFile.result.Result;
import com.lh.schedule.basisFile.result.Result;
import com.lh.schedule.businessFile.shopping.entity.Cation;
import com.lh.schedule.businessFile.shopping.entity.Cation;
import com.lh.schedule.businessFile.shopping.service.ICationService;
import com.lh.schedule.businessFile.shopping.service.ICationService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.wildfly.common.annotation.NotNull;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author LiuHuan
 * @since 2021-12-09
 */
@RestController
@RequestMapping("/shopping/cation")
@Api("分类模块")
public class CationController {
    @Autowired
    private ICationService iCationService;

    // 根据分类id查询关系图
    @PostMapping("selectByIdTree")
    public Result selectByIdTree(@NotNull String id){
        return Result.success(iCationService.selectByIdTree(id));
    }

    // 添加分类
    @PostMapping("insertCation")
    // @SysLog(oneLevel = "分类操作",towLevel = "新增分类",requestType = OperationType.INSERT)
    public Result insertCation(Cation cation){
        return Result.success(iCationService.insert(cation));
    }

    // 查询分类
    @PostMapping("selectCation")
    // @SysLog(oneLevel = "分类操作",towLevel = "根据条件查询分类",requestType = OperationType.SELECT)
    public Result selectCation(Cation cation){
        return Result.success(iCationService.selectCation(cation));
    }

    // 根据分类Id进行查询
    @PostMapping("selectId")
    // @SysLog(oneLevel = "分类操作",towLevel = "根据id查询分类",requestType = OperationType.SELECT)
    public Cation selectIdOne(String id){
        return iCationService.selectId(id);
    }


    // 根据分类Id进行修改分类
    @PostMapping("updateCation")
    // @SysLog(oneLevel = "分类操作",towLevel = "修改分类",requestType = OperationType.UPDATE)
    public Result updateCation(Cation Cation){
        return Result.success(iCationService.updateId(Cation));
    }

    // 根据分类Id进行删除分类
    @PostMapping("deleteId")
    // @SysLog(oneLevel = "分类操作",towLevel = "根据id删除分类",requestType = OperationType.DELETE)
    public Result deleteId(String id){
        return Result.success(iCationService.deleteId(id));
    }
}
