package com.lh.schedule.basisFile.config;

import com.lh.schedule.basisFile.config.vo.FlywayProperties;
import lombok.extern.slf4j.Slf4j;
import org.flywaydb.core.Flyway;
import org.flywaydb.core.api.FlywayException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.util.Map;

@Configuration
@Slf4j
public class FlywayConfig {

    @Autowired
    private DataSource dataSource;

    @Autowired
    private FlywayProperties flywayProperties;

    @PostConstruct
    public void migrate() {
        Map<Object, Object> flywayConfig = flywayProperties.getFlywayConfig();

        if(!(Boolean)flywayProperties.getFlywayConfig().get("switch")){
            log.info("数据全自动同步已关闭(建议开启)");
        }else{
            log.info("开始校验数据库版本信息");
            Flyway flywayInit = Flyway.configure().dataSource(dataSource)
                    .locations(String.valueOf(flywayConfig.get("locations")))
                    .baselineOnMigrate((Boolean) flywayConfig.get("baseline-on-migrate"))
                    .validateOnMigrate((Boolean) flywayConfig.get("validate-on-migrate"))
                    .table(String.valueOf(flywayConfig.get("table")))
                    .load();
            try{

                flywayInit.migrate();
                log.info("数据库版本更新完成");

            }catch (Exception e) {
                flywayInit.repair();
                log.error("遇到错误,启用备用方案");

                Flyway flywayInitTow = Flyway.configure()
                        .dataSource(dataSource)
                        .outOfOrder(true)
                        .locations(String.valueOf(flywayConfig.get("locations")))
                        .baselineOnMigrate((Boolean) flywayConfig.get("baseline-on-migrate"))
                        .validateOnMigrate((Boolean) flywayConfig.get("validate-on-migrate"))
                        .table(String.valueOf(flywayConfig.get("table")))
                        .load();
                flywayInitTow.migrate();

                log.info("解决成功");
            }
        }
    }
}