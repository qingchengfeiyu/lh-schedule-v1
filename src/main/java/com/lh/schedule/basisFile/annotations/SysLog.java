package com.lh.schedule.basisFile.annotations;


import com.lh.schedule.basisFile.enums.OperationType;

import java.lang.annotation.*;

/**
 * @author Administrator
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD,ElementType.TYPE})
@Documented
public @interface SysLog {
    String oneLevel() default "";
    String towLevel() default "";
    OperationType requestType() default OperationType.UNKNOWN;
    String remark() default "";
}
