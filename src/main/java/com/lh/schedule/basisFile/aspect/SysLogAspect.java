package com.lh.schedule.basisFile.aspect;


import com.lh.schedule.basisFile.annotations.SysLog;
import com.lh.schedule.basisFile.result.Result;
import com.lh.schedule.basisFile.result.Result;
import com.lh.schedule.businessFile.basis.entity.SysErrorLogEntity;
import com.lh.schedule.businessFile.basis.entity.SysLogEntity;
import com.lh.schedule.businessFile.basis.service.IErrorLogService;
import com.lh.schedule.businessFile.basis.service.ISysLogService;
import com.lh.schedule.businessFile.utli.SystemTools;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Component
@Aspect
@Slf4j
public class SysLogAspect {

    @Autowired
    private ISysLogService iSysLogService;

    @Autowired
    private IErrorLogService iErrorLogService;

    private HttpServletRequest request;

    private static long startTime = 0L;

//    @Pointcut("@annotation(io.swagger.annotations.ApiOperation)")
    @Pointcut("@annotation(com.lh.schedule.basisFile.annotations.SysLog)")
    public void pointCut() {
    }

//    在切点之前，织入相关代码
    @Before("pointCut() && @annotation(sysLog)")
    public void doBefore(JoinPoint joinPoint, SysLog sysLog) {
        request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();

        System.out.println("\033[97;42m" +"| info | "+ SystemTools.getIPAddress(request)+" ==========| "+sysLog.oneLevel()+" | "+sysLog.towLevel()+" | "+joinPoint.getSignature().getDeclaringTypeName()+"."+joinPoint.getSignature().getName()+" | "+ Arrays.toString(joinPoint.getArgs()) +" |========="+"\033[m");
    }

//    在切点之后，织入相关代码
    @After("pointCut()")
    public void doAfter(JoinPoint joinPoint) {

    }

//    在切点返回内容后，织入相关代码，一般用于对返回值做些加工处理的场景
    @AfterReturning(pointcut = "pointCut() && @annotation(sysLog)", returning = "result")
    public void doAfterReturning(JoinPoint joinPoint,SysLog sysLog, Object result) {
        request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        System.out.println("\033[93;42m" +"| info | "+SystemTools.getIPAddress(request)+" ==========| "+sysLog.oneLevel()+" | "+sysLog.towLevel()+" | "+joinPoint.getSignature().getDeclaringTypeName()+"."+joinPoint.getSignature().getName()+" | "+ Arrays.toString(joinPoint.getArgs()) +" | 用时:"+((System.currentTimeMillis()-startTime)+"ms")+" | 结果:"+ (result!=null?result.toString().length()>200?result.toString().substring(0, 200):result.toString():"")+"... |========="+"\033[m");


        SysLogEntity sysLogEntity = new SysLogEntity();

        sysLogEntity.setClassName(joinPoint.getSignature().getDeclaringTypeName());
        sysLogEntity.setMethodName(joinPoint.getSignature().getName());
        sysLogEntity.setOneLevel(sysLog.oneLevel().equals("")?joinPoint.getSignature().getDeclaringTypeName():sysLog.oneLevel());
        sysLogEntity.setTowLevel(sysLog.towLevel().equals("")?joinPoint.getSignature().getName():sysLog.towLevel());

        sysLogEntity.setParams(Arrays.toString(joinPoint.getArgs()).length()>1000?Arrays.toString(joinPoint.getArgs()).substring(0,1000):Arrays.toString(joinPoint.getArgs()));

        sysLogEntity.setRequestType(sysLog.requestType().name());
        if(result!=null){
            try {
                sysLogEntity.setResults(((Result) result).getData().toString().length()>1000?((Result) result).getData().toString().substring(0,1000):((Result) result).getData().toString());
            }catch (Exception e){
                sysLogEntity.setResults(result.toString().length()>1000?result.toString().substring(0, 1000):result.toString());
            }
        }

        sysLogEntity.setWhenTime(((System.currentTimeMillis()-startTime)+"ms"));

        iSysLogService.save(sysLogEntity);
    }

//    用来处理当织入的代码抛出异常后的逻辑处理
    @Order(1)
    @AfterThrowing(pointcut = "pointCut() && @annotation(sysLog)",throwing = "e")
    public void doAfterThrowing(JoinPoint joinPoint,SysLog sysLog,Throwable e) {
        request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();


        List<SysErrorLogEntity> listAll = iErrorLogService.lambdaQuery().eq(SysErrorLogEntity::getErrorText, e.getMessage()).list();

        List<SysErrorLogEntity> solutionAll=listAll.stream().filter(oneEntity-> oneEntity.getTreatmentScheme()!=null && !oneEntity.getTreatmentScheme().equals("")).collect(Collectors.toList());

        List<SysErrorLogEntity> collect = listAll.stream().filter(oneEntity -> joinPoint.getSignature().getDeclaringTypeName().equals(oneEntity.getClassName())
                && joinPoint.getSignature().getName().equals(oneEntity.getMethodName())
        &&oneEntity.getRequestType().equals(sysLog.requestType().name()
        )).collect(Collectors.toList());


        SysErrorLogEntity errorLogEntity = new SysErrorLogEntity();

        if(collect.size()!=0){
            errorLogEntity.setId(collect.get(0).getId());

            errorLogEntity.setNumber(collect.get(0).getNumber()+1);

        }else{
            errorLogEntity.setNumber(1);

        }

        errorLogEntity.setClassName(joinPoint.getSignature().getDeclaringTypeName());
        errorLogEntity.setMethodName(joinPoint.getSignature().getName());
        errorLogEntity.setOneLevel(sysLog.oneLevel());
        errorLogEntity.setTowLevel(sysLog.towLevel());
        errorLogEntity.setRequestType(sysLog.requestType().name());
        errorLogEntity.setErrorText(e.getMessage());
        iErrorLogService.saveOrUpdate(errorLogEntity);

        System.out.println("\033[97;41m" +"| error | "+SystemTools.getIPAddress(request)+" ==========| "+sysLog.oneLevel()+" | "+sysLog.towLevel()+" | "+joinPoint.getSignature().getDeclaringTypeName()+"."+joinPoint.getSignature().getName()+" | "+joinPoint.getArgs()+" | 用时: "+((System.currentTimeMillis()-startTime)+"ms")+" | 异常信息: "+e.getMessage()+"  |========= |"+"\033[m");

        if(listAll.size()>0){
            for (SysErrorLogEntity logEntity : listAll) {
                System.out.println("\033[93;41m" +"| ==历史出错信息列表== | "+logEntity.getCreateDate()+" | "+logEntity.getCreateUser()+" | "+logEntity.getCreateUserIp()+" | "+logEntity.getOneLevel()+" | "+logEntity.getTowLevel()+" | "+logEntity.getClassName()+"."+logEntity.getMethodName()+" | "+logEntity.getRequestType()+" | 方案："+logEntity.getTreatmentScheme()+" | 次数："+logEntity.getNumber()+" |"+"\033[m");
            }
        }

        System.out.println("\033[97;41m" +"| error | "+(solutionAll.size()>0?"历史处理方案":"暂无历史处理记录")+" | "+Arrays.toString(solutionAll.stream().map(SysErrorLogEntity::getTreatmentScheme).collect(Collectors.toList()).toArray())+" | ===== |"+"\033[m");
    }

    @Order(2)
    @Around("pointCut() && @annotation(apiOperation)")
    public Object doAround(ProceedingJoinPoint proceedingJoinPoint, ApiOperation apiOperation) {
        startTime=System.currentTimeMillis();

        Object obj;
        try {
            obj= proceedingJoinPoint.proceed();
        } catch (Throwable e) {
            e.printStackTrace();
//            return Result.failure(e.getMessage());
            throw new RuntimeException("接口异常:"+e.getMessage());
        }

        startTime=0L;

        return obj;
    }
}
