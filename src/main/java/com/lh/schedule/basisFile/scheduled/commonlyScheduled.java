package com.lh.schedule.basisFile.scheduled;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class commonlyScheduled {

    //    @Scheduled(fixedRate = 5000)
    @Scheduled(cron="0 */10 * * * ?")
    public void insertClock() {
        System.out.println("服务正常运行:NowTime:" +  new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));
    }
}
