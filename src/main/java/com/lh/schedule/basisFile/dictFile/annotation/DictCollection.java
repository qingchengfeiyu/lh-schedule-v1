package com.lh.schedule.basisFile.dictFile.annotation;

import java.lang.annotation.*;

/**
 * 标记没有集合字段需要进行字典翻译
 * @author: 刘欢
 **/
@Target(value = {ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface DictCollection {
}
