package com.lh.schedule.basisFile.dictFile.annotation;

import java.lang.annotation.*;

/**
 * @author: 刘欢
 **/
@Target(value = {ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface DictMap {

    DictMapper[] value();
}
