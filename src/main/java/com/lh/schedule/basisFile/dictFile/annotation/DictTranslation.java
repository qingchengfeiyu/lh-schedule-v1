package com.lh.schedule.basisFile.dictFile.annotation;

import java.lang.annotation.*;

/**
 * 标记方法返回值，将进行字典自动翻译.
 * @author: 刘欢
 **/
@Target(value = {ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface DictTranslation {
}
