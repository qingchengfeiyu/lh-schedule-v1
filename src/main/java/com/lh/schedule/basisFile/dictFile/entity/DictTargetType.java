package com.lh.schedule.basisFile.dictFile.entity;

/**
 * @author: 刘欢
 **/
public enum DictTargetType {
    /**
     * 字段类型
     */
    FIELD,
    /**
     * 实体类
     */
    ENTITY,
    /**
     * 实体类集合
     */
    COLLECTION;
}
