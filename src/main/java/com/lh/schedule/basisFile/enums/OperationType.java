package com.lh.schedule.basisFile.enums;

import lombok.AllArgsConstructor;
import lombok.ToString;

@ToString
@AllArgsConstructor
public enum OperationType {

    /**
     * 操作类型
     */
    UNKNOWN("unknown"),
//    删除
    DELETE("delete"),
//    查询
    SELECT("select"),
//    修改
    UPDATE("update"),
//    新增
    INSERT("insert");

    private final String value;

}
