package com.lh.schedule.basisFile.lineRunner;

import com.alibaba.fastjson.JSONObject;
import com.lh.schedule.basisFile.cache.DictCache;
import com.lh.schedule.businessFile.basis.entity.SysDict;
import com.lh.schedule.businessFile.basis.service.ISysDictService;
import com.lh.schedule.businessFile.utli.RedisUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
@Slf4j
public class CommandLineRunnerImpl implements CommandLineRunner {

    @Autowired
    DictCache dictionary;

    @Value("${spring.profiles.active}")
    private String active;

    @Override
    @DependsOn("flywayConfig")
    public void run(String... args){
        dictionary.iSysDictList().parallelStream().collect(Collectors.groupingBy(SysDict::getTypeCode)).forEach((key,value)->{
            System.out.println(key+":"+value.stream().map(SysDict::getName).collect(Collectors.toList()));
        });

        log.info("初始化字典完成");

        if(active.contains("dev")){
            log.info("开启swagger2增强版: host:port/doc.html ");
        }
    }
}
