package com.schedule.lhbasis.framework.aspect;

import com.schedule.lhbasis.framework.enums.OperationType;
import com.schedule.lhbasis.entity.SysErrorLogEntity;
import com.schedule.lhbasis.entity.SysLogEntity;
import com.schedule.lhbasis.service.IErrorLogService;
import com.schedule.lhbasis.service.ISysLogService;
import com.schedule.lhbasis.utli.SystemTools;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Component
@Aspect
@Slf4j
public class ApiLogAspect {

    @Autowired
    private ISysLogService iSysLogService;

    @Autowired
    private IErrorLogService iErrorLogService;

    private HttpServletRequest request;

    private static long startTime = 0L;

    @Pointcut(value = "@within(io.swagger.annotations.Api)")
//    @Pointcut(value = "@within(org.springframework.web.bind.annotation.RestController)||@annotation(org.springframework.web.bind.annotation.RestController)")
    public void cutManageLoginService() {

    }

    //    用来处理当织入的代码抛出异常后的逻辑处理
    @Order(1)
    @AfterThrowing(pointcut = "cutManageLoginService()",throwing = "e")
    public void doAfterThrowing(JoinPoint joinPoint, Throwable e) {

        //        获取方法注解
        ApiOperation sysLog = this.getApiOperation((ProceedingJoinPoint) joinPoint);
//        获取类注解
        Api api=this.getApi((ProceedingJoinPoint) joinPoint);

        String describe= OperationType.UNKNOWN.name();

        String oneLevel= joinPoint.getSignature().getDeclaringTypeName();
        String twoLevel=joinPoint.getSignature().getName();
        if(api!=null && api.value()!=null){
            oneLevel= api.value();
        }
        if(sysLog!=null && sysLog.notes()!=null){
            describe=sysLog.notes();
        }
        if(sysLog!=null && sysLog.value()!=null){
            twoLevel= sysLog.value();
        }

        request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();

        List<SysErrorLogEntity> listAll = iErrorLogService.lambdaQuery().eq(SysErrorLogEntity::getErrorText, e.getMessage()).list();

        List<SysErrorLogEntity> solutionAll=listAll.parallelStream().filter(oneEntity-> oneEntity.getTreatmentScheme()!=null && !oneEntity.getTreatmentScheme().equals("")).collect(Collectors.toList());

        String finalDescribe = describe;

        List<SysErrorLogEntity> collect = listAll.parallelStream().filter(oneEntity -> joinPoint.getSignature().getDeclaringTypeName().equals(oneEntity.getClassName())
                && joinPoint.getSignature().getName().equals(oneEntity.getMethodName())
                &&oneEntity.getRequestType().equals(finalDescribe))
                .collect(Collectors.toList());


        SysErrorLogEntity errorLogEntity = new SysErrorLogEntity();

        if(collect.size()!=0){
            errorLogEntity.setId(collect.get(0).getId());

            errorLogEntity.setNumber(collect.get(0).getNumber()+1);

        }else{
            errorLogEntity.setNumber(1);

        }

        errorLogEntity.setClassName(joinPoint.getSignature().getDeclaringTypeName());
        errorLogEntity.setMethodName(joinPoint.getSignature().getName());
        errorLogEntity.setOneLevel(oneLevel);
        errorLogEntity.setTowLevel(twoLevel);
        errorLogEntity.setRequestType(describe);
        errorLogEntity.setErrorText(e.getMessage());
        iErrorLogService.saveOrUpdate(errorLogEntity);

        System.out.println("\033[97;41m" +"| error | "+ SystemTools.getIPAddress(request)+" ==========| "+oneLevel+" | "+twoLevel+" | "+joinPoint.getSignature().getDeclaringTypeName()+"."+joinPoint.getSignature().getName()+" | "+joinPoint.getArgs()+" | 用时: "+((System.currentTimeMillis()-startTime)+"ms")+" | 异常信息: "+e.getMessage()+"  |========= |"+"\033[m");

        if(listAll.size()>0){
            for (SysErrorLogEntity logEntity : listAll) {
                System.out.println("\033[93;41m" +"| ==历史出错信息列表== | "+logEntity.getCreateDate()+" | "+logEntity.getCreateUser()+" | "+logEntity.getCreateUserIp()+" | "+logEntity.getOneLevel()+" | "+logEntity.getTowLevel()+" | "+logEntity.getClassName()+"."+logEntity.getMethodName()+" | "+logEntity.getRequestType()+" | 方案："+logEntity.getTreatmentScheme()+" | 次数："+logEntity.getNumber()+" |"+"\033[m");
            }
        }

        System.out.println("\033[97;41m" +"| error | "+(solutionAll.size()>0?"历史处理方案":"暂无历史处理记录")+" | "+Arrays.toString(solutionAll.stream().map(SysErrorLogEntity::getTreatmentScheme).collect(Collectors.toList()).toArray())+" | ===== |"+"\033[m");

    }

    @Order(2)
    @Around("cutManageLoginService()")
    public Object ManageLoginService(ProceedingJoinPoint point) throws Throwable {

//        获取方法注解
        ApiOperation sysLog = this.getApiOperation(point);
//        获取类注解
        Api api=this.getApi(point);

        String oneLevel= point.getSignature().getDeclaringTypeName();
        String twoLevel=point.getSignature().getName();
        if(api!=null && api.value()!=null){
            oneLevel= api.value();
        }
        if(sysLog!=null && sysLog.value()!=null){
            twoLevel= sysLog.value();
        }


        startTime=System.currentTimeMillis();

        request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();

        System.out.println("\033[97;42m" +"| info | "+ SystemTools.getIPAddress(request)+" ==========| "+
                oneLevel+" | "+twoLevel+
                " | "+point.getSignature().getDeclaringTypeName()+"."+point.getSignature().getName()+" | "+
                Arrays.toString(point.getArgs()) +" |========="+"\033[m");

        Object obj;
        try {
            obj= point.proceed();

            System.out.println("\033[93;42m" +"| info | "+SystemTools.getIPAddress(request)+" ==========| "+oneLevel+" | "
                    +twoLevel+" | "
                    +point.getSignature().getDeclaringTypeName()+"."+point.getSignature().getName()+" | "+ Arrays.toString(point.getArgs())
                    +" | 用时:"+((System.currentTimeMillis()-startTime)+"ms")+" | 结果:"
                    + (obj!=null?obj.toString().length()>200?obj.toString().substring(0, 200):obj.toString():"")+"... |========="+"\033[m");

            this.successRun(point,sysLog,api,obj);

        } catch (Throwable e) {
            e.printStackTrace();
//            return Result.failure(e.getMessage());
            throw new RuntimeException("接口异常:"+e.getMessage());
        }

        startTime=0L;
        return obj;
    }


    public void successRun(ProceedingJoinPoint point, ApiOperation sysLog, Api api,Object obj){


        String describe= OperationType.UNKNOWN.name();

        String oneLevel= point.getSignature().getDeclaringTypeName();
        String twoLevel=point.getSignature().getName();
        if(api!=null && api.value()!=null){
            oneLevel= api.value();
        }
        if(sysLog!=null && sysLog.notes()!=null){
            describe=sysLog.notes();
        }
        if(sysLog!=null && sysLog.value()!=null){
            twoLevel= sysLog.value();
        }


        SysLogEntity sysLogEntity = new SysLogEntity();

        sysLogEntity.setClassName(point.getSignature().getDeclaringTypeName());
        sysLogEntity.setMethodName(point.getSignature().getName());
        sysLogEntity.setOneLevel(oneLevel);
        sysLogEntity.setTowLevel(twoLevel);

        sysLogEntity.setParams(Arrays.toString(point.getArgs()).length()>1000?Arrays.toString(point.getArgs()).substring(0,1000):Arrays.toString(point.getArgs()));

        sysLogEntity.setRequestType(describe);

        if(obj!=null && !StringUtils.isEmpty(obj.toString())){
            sysLogEntity.setResults(obj.toString().length()>1000?obj.toString().substring(0,1000):obj.toString());
        }

        sysLogEntity.setWhenTime(((System.currentTimeMillis()-startTime)+"ms"));

        iSysLogService.save(sysLogEntity);
    }

    private Api getApi(ProceedingJoinPoint point){

        // 获取切入的 Method
        MethodSignature joinPointObject = (MethodSignature) point.getSignature();
        Method method = joinPointObject.getMethod();

        boolean flag = method.isAnnotationPresent(Api.class);
        if (flag) {
            Api annotation = method.getAnnotation(Api.class);
            return annotation;
        } else {
            // 如果方法上没有注解，则搜索类上是否有注解
            Api classAnnotation = AnnotationUtils.findAnnotation(joinPointObject.getMethod().getDeclaringClass(), Api.class);
            if (classAnnotation != null) {
                return classAnnotation;
            } else {
                return null;
            }
        }
    }

    private ApiOperation getApiOperation(ProceedingJoinPoint point){

        // 获取切入的 Method
        MethodSignature joinPointObject = (MethodSignature) point.getSignature();
        Method method = joinPointObject.getMethod();

        boolean flag = method.isAnnotationPresent(ApiOperation.class);
        if (flag) {
            ApiOperation annotation = method.getAnnotation(ApiOperation.class);
            return annotation;
        } else {
            // 如果方法上没有注解，则搜索类上是否有注解
            ApiOperation classAnnotation = AnnotationUtils.findAnnotation(joinPointObject.getMethod().getDeclaringClass(), ApiOperation.class);
            if (classAnnotation != null) {
                return classAnnotation;
            } else {
                return null;
            }
        }
    }

}
