package com.schedule.lhbasis.framework.cache;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.schedule.lhbasis.entity.SysDict;
import com.schedule.lhbasis.framework.dictFile.service.DictCacheService;
import com.schedule.lhbasis.mapper.SysDictMapper;
import com.schedule.lhbasis.service.ISysDictService;
import com.schedule.lhbasis.utli.RedisUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.DependsOn;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
@DependsOn("flywayConfig")
public class DictCache implements DictCacheService {

    @Autowired
    private RedisUtils redisUtils;

    @Autowired
    private ISysDictService iSysDictService;

    @Autowired
    private SysDictMapper sysDictMapper;

    @Autowired
    private RedisTemplate redisTemplate;

//    注入字典信息
    public List<SysDict> dictList;

    @Value("${lh-config.automatic.translation.redis-cache:#{false}}")
    private boolean redisCache;

    public List<SysDict> iSysDictList() {
//        设置redis缓存
        redisUtils.set("sysDictList", JSONObject.toJSONString(iSysDictService.list()));

        dictList=iSysDictService.list();
        return dictList;
    }


    @Override
    public BiMap<String, String> getDictMapByName(String dictName) {
        BiMap<String, String> dictMap =HashBiMap.create();

        try {
            if(redisCache){
//              缓存中查询
                dictMap=getDictMapRedis(dictName);
            }else{
//            内存中查询
                dictMap=getDictMap(dictName);
            }

            if(dictMap.isEmpty()){
                dictMap=getDictMapSql(dictName);
            }

        }catch (Exception e) {
            System.err.println("======  |"+dictName+"| ======== | 所查字典项为null");
        }
        return dictMap;

    }

    public BiMap<String, String> getDictMapSql(String dictName) {
        BiMap<String, String> dictMap =HashBiMap.create();
        dictMap.putAll( iSysDictService.lambdaQuery()
                .eq(SysDict::getTypeCode, dictName)
                .and(sysDictLambdaQueryWrapper -> sysDictLambdaQueryWrapper
                        .ne(SysDict::getSuperId, "")
                        .or()
                        .ne(SysDict::getSuperId, null))
                .list()
                .stream()
                .collect(Collectors.toMap(SysDict::getValue, SysDict::getName))
        );

        return dictMap;
    }

    public BiMap<String, String> getDictMap(String dictName) {
        BiMap<String, String> dictMap =HashBiMap.create();
        Map<String, String> collect = dictList.stream()
                .filter(dict -> dictName.equals(dict.getTypeCode())
                        && !(StringUtils.isEmpty(dict.getSuperId())))
                .collect(Collectors.toMap(SysDict::getValue, SysDict::getName));

        dictMap.putAll(collect);
        return dictMap;
    }


    public BiMap<String, String> getDictMapRedis(String dictName) {
        BiMap<String, String> dictMap =HashBiMap.create();

        List<SysDict> sysDictList = JSONArray.parseArray(redisUtils.get("sysDictList").toString(),SysDict.class);
        Map<String, String> collect = sysDictList.stream()
                .filter(dict -> dictName.equals(dict.getTypeCode())
                        && !(StringUtils.isEmpty(dict.getSuperId())))
                .collect(Collectors.toMap(SysDict::getValue, SysDict::getName));


        dictMap.putAll(collect);
        return dictMap;
    }
}