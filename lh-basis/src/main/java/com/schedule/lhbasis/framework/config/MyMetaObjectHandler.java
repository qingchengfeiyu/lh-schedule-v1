package com.schedule.lhbasis.framework.config;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.schedule.lhbasis.utli.SystemTools;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

@Component
public class MyMetaObjectHandler implements MetaObjectHandler {


    //使用mp实现添加操作，这个方法执行  createTime为字段  new Date()为现在时间  metaObject
    @Override
    public void insertFill(MetaObject metaObject) {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();

        this.setFieldValByName("createDate",new Date(),metaObject);
        this.setFieldValByName("createUser","张三",metaObject);
        this.setFieldValByName("createUserIp", SystemTools.getIPAddress(request),metaObject);
        this.setFieldValByName("logicDel",1,metaObject);
        this.setFieldValByName("version",0,metaObject);
    }

    //使用mp实现修改操作，这个方法执行
    @Override
    public void updateFill(MetaObject metaObject) {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();

        this.setFieldValByName("updateDate",new Date(),metaObject);
        this.setFieldValByName("updateUser","李四",metaObject);
        this.setFieldValByName("updateUserIp",SystemTools.getIPAddress(request),metaObject);
    }
}
