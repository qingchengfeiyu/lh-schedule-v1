package com.schedule.lhbasis.framework.dictFile.entity;

import com.google.common.collect.BiMap;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author: 刘欢
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DataDictDTO {
    private DictTargetType dictTargetType;
    /**
     * 原字段
     */
    private String sourceField;
    /**
     * 当前字段翻译使用的字典
     */
    private BiMap<String, String> dictDetail;
    /**
     * 目标字段，翻译的字段name的字段名称
     */
    private String targetField;
    /**
     * 是否是多个字典值拼接的形式
     */
    private boolean multiple;
    /**
     * 原字段是null时的缺省值
     */
    private String nullValue;
    /**
     * 找不到字典对应时的缺省值
     */
    private String undefinedValue;

    /**
     * 当翻译字段是实体类或者集合的时候，将每个字段的信息放到这个里面
     */
    private List<DataDictDTO> collectionDictInfo;

//
    public DataDictDTO(String sourceField, BiMap<String, String> dictDetail, String targetField, boolean multiple, String nullValue, String undefinedValue) {
        this.dictTargetType= DictTargetType.FIELD;
        this.sourceField = sourceField;
        this.dictDetail = dictDetail;
        this.targetField = targetField;
        this.multiple = multiple;
        this.nullValue = nullValue;
        this.undefinedValue = undefinedValue;
    }

    public DataDictDTO(DictTargetType dictTargetType, String sourceField, List<DataDictDTO> collectionDictInfo) {
        this.dictTargetType = dictTargetType;
        this.sourceField = sourceField;
        this.collectionDictInfo = collectionDictInfo;
    }
}
