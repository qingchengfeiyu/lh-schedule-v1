package com.schedule.lhbasis.framework.config.vo;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Data
@Configuration
@ConfigurationProperties(prefix = "lh-config")
public class FlywayProperties {

    private Map<Object, Object> flywayConfig = new HashMap<>();
}
