package com.schedule.lhbasis.framework.config.service.impl;//package com.schedule.lhbasis.framework.config.service.impl;
//
//import com.baomidou.mybatisplus.core.toolkit.Wrappers;
//import com.schedule.lhbasis.entity.SysRole;
//import com.schedule.lhbasis.entity.SysUser;
//import com.schedule.lhbasis.entity.SysUserRole;
//import com.schedule.lhbasis.service.ISysRoleService;
//import com.schedule.lhbasis.service.ISysUserRoleService;
//import com.schedule.lhbasis.service.ISysUserService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.core.GrantedAuthority;
//import org.springframework.security.core.authority.SimpleGrantedAuthority;
//import org.springframework.security.core.userdetails.User;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.security.core.userdetails.UserDetailsService;
//import org.springframework.security.core.userdetails.UsernameNotFoundException;
//import org.springframework.stereotype.Service;
//
//import java.util.ArrayList;
//import java.util.Collection;
//import java.util.List;
//
//@Service
//public class CustomUserDetailsServiceImpl implements UserDetailsService {
//    @Autowired
//    private ISysUserService iSysUserService;
//
//    @Autowired
//    private ISysRoleService iSysRoleService;
//
//    @Autowired
//    private ISysUserRoleService iSysUserRoleService;
//
//    @Override
//    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
//        Collection<GrantedAuthority> authorities = new ArrayList<>();
//        // 从数据库中取出用户信息
//        SysUser user = iSysUserService.getOne(Wrappers.<SysUser> lambdaQuery().eq(SysUser::getUserAccount,username));
//
//        // 判断用户是否存在
//        if(user == null) {
//            throw new UsernameNotFoundException("用户名不存在");
//        }
//
//        // 添加权限
//        List<SysUserRole> userRoles = iSysUserRoleService.list(Wrappers.<SysUserRole> lambdaQuery().eq(SysUserRole::getUserId,user.getId()));
//        for (SysUserRole userRole : userRoles) {
//            SysRole role = iSysRoleService.getById(userRole.getRoleId());
//            authorities.add(new SimpleGrantedAuthority(role.getRoleName()));
//        }
//
//        // 返回UserDetails实现类
//        return new User(user.getUserName(), user.getUserPassword(), authorities);
//    }
//}
//
