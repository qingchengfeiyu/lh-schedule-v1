package com.schedule.lhbasis.framework.annotations;


import com.schedule.lhbasis.framework.enums.OperationType;

import java.lang.annotation.*;

/**
 * @author Administrator
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD,ElementType.TYPE})
@Documented
public @interface SysLog {
    String oneLevel() default "";
    String towLevel() default "";
    OperationType requestType() default OperationType.UNKNOWN;
    String remark() default "";
}
