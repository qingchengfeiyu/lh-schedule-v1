package com.schedule.lhbasis.framework.cache;

import com.schedule.lhbasis.entity.SysDict;
import com.schedule.lhbasis.framework.dictFile.service.AlternateProcessingService;
import com.schedule.lhbasis.service.ISysDictService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class alternateProcessingCache implements AlternateProcessingService {

    @Autowired
    ISysDictService iSysDictService;

    @Autowired
    DictCache dictCache;

    @Autowired
    private RedisTemplate redisTemplate;

    @Override
    public String gatNameAndRefactoringMap(String type, String value) {
        dictCache.dictList = dictCache.iSysDictList();
        redisTemplate.opsForList().rightPushAll("SysDictList",dictCache.dictList);

        List<SysDict> sysDictList=dictCache.dictList.stream().filter(dict->!(StringUtils.isEmpty(dict.getValue()))).collect(Collectors.toList());

        try{
            return sysDictList.stream().filter(sysDict -> sysDict.getTypeCode().equals(type) && sysDict.getValue().equals(value)).map(SysDict::getName).collect(Collectors.toList()).get(0);
        }catch (Exception e) {
            return "";
        }
    }
}
