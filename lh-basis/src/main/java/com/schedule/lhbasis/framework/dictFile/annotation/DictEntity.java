package com.schedule.lhbasis.framework.dictFile.annotation;

import java.lang.annotation.*;

/**
 * 标记这个字典时一个进行字典翻译的实体类
 * @author: 刘欢
 **/
@Target(value = {ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface DictEntity {
}
