package com.schedule.lhbasis.framework.result;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

@Data
@Accessors(chain = true)
public class Result<T> implements Serializable {

    private Integer code;
    private String message;
    private T data;
    private final String timeStamp = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date());

    public static final String SUCCESS = "成功";
    public static final String FAILURE = "失败";


    public Result(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public Result(int code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public static Result<Object> success() {
        return new Result<Object>(200, SUCCESS);
    }

    public static Result<Object> success(Object data) {
        return new Result<Object>(200, SUCCESS, data);
    }

    public static Result<Object> failure() {
        return new Result<Object>(400, FAILURE);
    }

    public static Result<Object> failure(Object data) {
        return new Result<Object>(400, FAILURE, data);
    }

    public static Result<Object> failure(Object data, Integer sign) {
        return new Result<Object>(sign, FAILURE, data);
    }
}
