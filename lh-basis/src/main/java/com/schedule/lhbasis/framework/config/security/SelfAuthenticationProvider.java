package com.schedule.lhbasis.framework.config.security;//package com.schedule.lhbasis.framework.config.security;
//
//import com.baomidou.mybatisplus.core.toolkit.Wrappers;
//import com.schedule.lhbasis.entity.SysUser;
//import com.schedule.lhbasis.entity.SysUserRole;
//import com.schedule.lhbasis.service.ISysRoleService;
//import com.schedule.lhbasis.service.ISysUserRoleService;
//import com.schedule.lhbasis.service.ISysUserService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.authentication.AuthenticationProvider;
//import org.springframework.security.authentication.BadCredentialsException;
//import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
//import org.springframework.security.core.Authentication;
//import org.springframework.security.core.AuthenticationException;
//import org.springframework.security.core.GrantedAuthority;
//import org.springframework.security.core.authority.SimpleGrantedAuthority;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
//import org.springframework.stereotype.Component;
//
//import static com.fasterxml.jackson.databind.type.LogicalType.Collection;
//
////登录认证处理
//@Component
//public class SelfAuthenticationProvider implements AuthenticationProvider {
//
//    //DAO查询用户
//    @Autowired
//    private ISysUserService iSysUserService;
//
//    @Autowired
//    private ISysRoleService iSysRoleService;
//
//    @Autowired
//    private ISysUserRoleService iSysUserRoleService;
//
//    //密码加密解密
//    @Override
//    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
//
//        //表单输入的用户名
//        String username = (String) authentication.getPrincipal();
//        //表单输入的密码
//        String password = (String) authentication.getCredentials();
//        SysUser oneSysUser = iSysUserService.getOne(Wrappers.lambdaQuery(SysUser.class).eq(SysUser::getUserAccount, username));
//        //对加密密码进行验证
//        if (!new BCryptPasswordEncoder().matches(password, oneSysUser.getUserPassword())) {
//            throw new BadCredentialsException("密码错误");
//        } else {
//            return new UsernamePasswordAuthenticationToken(username, password, null);
//        }
//
//    }
//
//    @Override
//    public boolean supports(Class<?> aClass)   {
//        return true;
//    }
//}