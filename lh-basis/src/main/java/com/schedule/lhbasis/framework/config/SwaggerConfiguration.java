package com.schedule.lhbasis.framework.config;

import com.github.xiaoymin.knife4j.spring.annotations.EnableKnife4j;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
@EnableKnife4j
@Slf4j
public class SwaggerConfiguration {

    @Value("${lh-config.swagger-config.base-package:#{'com'}}")
    private String basePackage;

    @Value("${lh-config.swagger-config.base-name:#{'default'}}")
    private String baseName;

    @Bean
    public Docket createRestApiBasis() {
        return new Docket(DocumentationType.OAS_30)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.schedule.lhbasis"))
                .paths(PathSelectors.any())     //正则匹配请求路径，并分配至当前分组，当前所有接口
                .build()
                .groupName("基础模块信息")      //分组名称
                ;
    }

    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.OAS_30)
                .useDefaultResponseMessages(false)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage(basePackage))
                .paths(PathSelectors.any())
                .build()
                .groupName(baseName);
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .description("答案就在眼前")
                .contact(new Contact("刘欢 | 翟羽佳", "localhost:8081/doc.html", "l3390179542 | zyj18791510258"))
                .version("v1.0.2")
                .title("日复一日，成果总结")
                .build();
    }
}