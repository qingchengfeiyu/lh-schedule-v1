package com.schedule.lhbasis.framework.interceptor;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Slf4j
public class MyIntercaeptor implements HandlerInterceptor {

//    执行前拦截
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
//        log.info("preHandle执行");

        return HandlerInterceptor.super.preHandle(request, response, handler);
    }

//    执行完成后执行
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
//        log.info("postHandle执行");

        HandlerInterceptor.super.postHandle(request, response, handler, modelAndView);
    }

//    只有preHandle成功执行，并且返回ture后执行     视图渲染玩成后执行
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
//        log.info("afterCompletion执行");
        HandlerInterceptor.super.afterCompletion(request, response, handler, ex);
    }
}
