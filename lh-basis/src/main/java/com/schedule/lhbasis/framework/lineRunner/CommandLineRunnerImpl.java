package com.schedule.lhbasis.framework.lineRunner;

import com.schedule.lhbasis.framework.cache.DictCache;
import com.schedule.lhbasis.entity.SysDict;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Component
@Slf4j
public class CommandLineRunnerImpl implements CommandLineRunner {

    @Autowired
    DictCache dictionary;

    @Value("${spring.profiles.active}")
    private String active;

    @Value("${lh-config.swagger-config.base-package:#{'com.schedule'}}")
    private String basePackage;

    @Override
    @DependsOn("flywayConfig")
    public void run(String... args){
        dictionary.iSysDictList().parallelStream().collect(Collectors.groupingBy(SysDict::getTypeCode)).forEach((key,value)->{
            System.out.println(key+":"+value.stream().map(SysDict::getName).collect(Collectors.toList()));
        });

        log.info("初始化字典完成");

        if(active.contains("dev")){
            log.info("开启swagger2增强版: host:port/doc.html   扫描路径为："+basePackage);
            this.getBeanCons();
        }
    }

    @Autowired
    private ApplicationContext appContext;

//    @PostConstruct
    public void getBeanCons(){
        String[] serviceBean = appContext.getBeanNamesForAnnotation(Service.class);
        String[] controllerBean = appContext.getBeanNamesForAnnotation(Controller.class);
        String[] mapperBean = appContext.getBeanNamesForAnnotation(Mapper.class);

        System.out.println("\033[1;35m"+"装配的Controller如下："+ Arrays.toString(controllerBean) +"\033[m");
        System.out.println("\033[1;35m"+"装配的serviceImpl如下："+ Arrays.toString(serviceBean) +"\033[m");
        System.out.println("\033[1;35m"+"装配的mapper如下："+ Arrays.toString(mapperBean) +"\033[m");
    }
}
