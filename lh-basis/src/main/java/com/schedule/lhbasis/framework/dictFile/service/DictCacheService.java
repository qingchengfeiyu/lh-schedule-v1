package com.schedule.lhbasis.framework.dictFile.service;

import com.google.common.collect.BiMap;

/**
 * @author: 刘欢
 **/
public interface DictCacheService {

    BiMap<String, String> getDictMapByName(String dictName);

}
