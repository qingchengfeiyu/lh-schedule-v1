package com.schedule.lhbasis.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.schedule.lhbasis.entity.SysUser;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author LiuHuan
 * @since 2021-12-03
 */
public interface ISysUserService extends IService<SysUser> {

    boolean replaceTheName(SysUser applicant,SysUser otherParty);

    List<SysUser> listSysUser();
}
