package com.schedule.lhbasis.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.schedule.lhbasis.entity.SysUser;
import com.schedule.lhbasis.mapper.SysUserMapper;
import com.schedule.lhbasis.service.ISysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author LiuHuan
 * @since 2021-12-03
 */
@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements ISysUserService {

    @Autowired
    private SysUserMapper sysUserMapper;

//    替换角色名称信息
    @Override
    // @SysLog
    @Transactional
    public boolean replaceTheName(SysUser applicant, SysUser otherParty) {
        SysUser applicantOne = this.getById(applicant.getId());
        SysUser otherPartyOne = this.getById(otherParty.getId());
        System.out.println("申请人信息"+applicantOne);
        System.out.println("目标人信息"+otherPartyOne);

        String applicantName=applicantOne.getUserName();
        String otherPartytName=otherPartyOne.getUserName();

        System.out.println("正在替换  申请人 :"+applicantName+" ->  目标人 : "+otherPartytName);

//        applicantName=otherPartytName+((otherPartytName=applicantName)==""?"":"");
        applicantOne.setUserName(otherPartytName);
        this.updateById(applicantOne);

        int i = 1/0;

        System.out.println("正在替换  目标人 :"+otherPartytName+" ->  申请人 : "+applicantName);

        otherPartyOne.setUserName(applicantName);
        this.updateById(otherPartyOne);

        return true;
    }

//    原生查询所有用户信息
    @Override
    public List<SysUser> listSysUser() {
        return sysUserMapper.sysUserList();
    }
}
