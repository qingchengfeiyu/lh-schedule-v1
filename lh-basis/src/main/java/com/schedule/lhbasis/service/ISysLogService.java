package com.schedule.lhbasis.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.schedule.lhbasis.entity.SysLogEntity;

public interface ISysLogService extends IService<SysLogEntity> {
}
