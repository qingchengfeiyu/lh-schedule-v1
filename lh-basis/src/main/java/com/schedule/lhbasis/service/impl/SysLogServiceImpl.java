package com.schedule.lhbasis.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.schedule.lhbasis.entity.SysLogEntity;
import com.schedule.lhbasis.mapper.SysLogMapper;
import com.schedule.lhbasis.service.ISysLogService;
import org.springframework.stereotype.Service;

@Service
public class SysLogServiceImpl extends ServiceImpl<SysLogMapper, SysLogEntity> implements ISysLogService {
}
