package com.schedule.lhbasis.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.schedule.lhbasis.entity.SysErrorLogEntity;

public interface IErrorLogService extends IService<SysErrorLogEntity> {
}
