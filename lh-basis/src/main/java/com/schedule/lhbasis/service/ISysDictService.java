package com.schedule.lhbasis.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.schedule.lhbasis.entity.SysDict;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author liuhuan
 * @since 2021-11-19
 */
public interface ISysDictService extends IService<SysDict> {

    List<SysDict> redisTest(Integer t);
}
