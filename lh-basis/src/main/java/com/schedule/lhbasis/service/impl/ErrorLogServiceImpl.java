package com.schedule.lhbasis.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.schedule.lhbasis.entity.SysErrorLogEntity;
import com.schedule.lhbasis.mapper.ErrorLogMapper;
import com.schedule.lhbasis.service.IErrorLogService;
import org.springframework.stereotype.Service;

@Service
public class ErrorLogServiceImpl  extends ServiceImpl<ErrorLogMapper, SysErrorLogEntity> implements IErrorLogService {
}
