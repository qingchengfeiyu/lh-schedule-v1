package com.schedule.lhbasis.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.schedule.lhbasis.entity.SysUserRole;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author LiuHuan
 * @since 2021-12-06
 */
public interface ISysUserRoleService extends IService<SysUserRole> {

}
