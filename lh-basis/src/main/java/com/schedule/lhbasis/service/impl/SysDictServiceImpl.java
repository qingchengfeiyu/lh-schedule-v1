package com.schedule.lhbasis.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.schedule.lhbasis.entity.SysDict;
import com.schedule.lhbasis.mapper.SysDictMapper;
import com.schedule.lhbasis.service.ISysDictService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author kenx
 * @since 2021-11-19
 */
@Service
public class SysDictServiceImpl extends ServiceImpl<SysDictMapper, SysDict> implements ISysDictService {

    @Autowired
    ISysDictService iSysDictService;

    @Override
    //表示使用my-redis-cache1缓存空间，key的生成策略为redis+t，当t>10的时候才会使用缓存
    @Cacheable(value = "my-redis-cache1")
    public List<SysDict> redisTest(Integer t) {
        return iSysDictService.list();
    }
}
