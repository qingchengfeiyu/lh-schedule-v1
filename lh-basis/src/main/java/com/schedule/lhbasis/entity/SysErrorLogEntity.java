package com.schedule.lhbasis.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.schedule.lhbasis.common.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@TableName("sys_log_error")
public class SysErrorLogEntity extends BaseEntity implements Serializable {

    //    类名
    @TableField("CLASS_NAME")
    private String className;

    //    方法名
    @TableField("METHOD_NAME")
    private String methodName;

    //    请求类型
    @TableField("REQUEST_TYPE")
    private String requestType;

    //    一级目录
    @TableField("ONE_LEVEL")
    private String oneLevel;

    //    二级目录
    @TableField("TOW_LEVEL")
    private String towLevel;

    //    报错内容
    @TableField("ERROR_TEXT")
    private String errorText;

    //    处理方案
    @TableField("TREATMENT_SCHEME")
    private String treatmentScheme;

    //    此处异常次数
    @TableField("NUMBER")
    private int number;

}
