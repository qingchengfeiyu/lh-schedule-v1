package com.schedule.lhbasis.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.schedule.lhbasis.common.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author LiuHuan
 * @since 2021-12-06
 */
@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class SysRole  extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableField("RESERVED1")
    private String reserved1;

    @TableField("RESERVED2")
    private String reserved2;

    @TableField("RESERVED3")
    private String reserved3;

    @TableField("RESERVED4")
    private String reserved4;

    @TableField("RESERVED5")
    private String reserved5;

    /**
     * 名称
     */
    @TableField("ROLE_NAME")
    private String roleName;

    /**
     * 类型
     */
    @TableField("ROLE_TYPE")
    private String roleType;


}
