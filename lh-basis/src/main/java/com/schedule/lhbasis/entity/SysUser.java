package com.schedule.lhbasis.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.schedule.lhbasis.framework.dictFile.annotation.Dict;
import com.schedule.lhbasis.common.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * <p>
 * 
 * </p>
 *
 * @author LiuHuan
 * @since 2021-12-03
 */
@Data
@SuperBuilder
@ToString(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class SysUser extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 名称
     */
    @TableField("USER_NAME")
    private String userName;

    /**
     * 性别
     */
    @TableField("USER_SEX")
//    这个注解标识翻译字典中的类型SEX  默认找当前字段名拼接NAME 咱们可以自定义   multiple=true表示开启多段翻译    nullValueName是空处理  undefinedValue内容不存在处理
    @Dict(dictName = "SEX",targetField = "userSexName",multiple = true,nullValueName = "我是傻逼",undefinedValue = "我爱吃屎")
    private String userSex;

    @TableField(exist = false)
    private String userSexName;


    /**
     * 密码
     */
    @TableField("USER_PASSWORD")
    private String userPassword;

    /**
     * 生日
     */
    @TableField("USER_DIRTHDAY")
    private LocalDate userDirthday;

    @TableField("RESERVED1")
    private String reserved1;

    @TableField("RESERVED2")
    private String reserved2;

    @TableField("RESERVED3")
    private String reserved3;

    @TableField("RESERVED4")
    private String reserved4;

    @TableField("RESERVED5")
    private String reserved5;

    @TableField("USER_ACCOUNT")
    private String userAccount;
}
