package com.schedule.lhbasis.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.schedule.lhbasis.entity.SysErrorLogEntity;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ErrorLogMapper extends BaseMapper<SysErrorLogEntity> {
}
