package com.schedule.lhbasis.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.schedule.lhbasis.entity.SysLogEntity;

public interface SysLogMapper extends BaseMapper<SysLogEntity> {
}
