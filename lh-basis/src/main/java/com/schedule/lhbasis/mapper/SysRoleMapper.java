package com.schedule.lhbasis.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.schedule.lhbasis.entity.SysRole;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author LiuHuan
 * @since 2021-12-06
 */
public interface SysRoleMapper extends BaseMapper<SysRole> {

}
