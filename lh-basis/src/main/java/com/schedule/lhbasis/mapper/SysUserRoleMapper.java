package com.schedule.lhbasis.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.schedule.lhbasis.entity.SysUserRole;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author LiuHuan
 * @since 2021-12-06
 */
public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {

}
