package com.schedule.lhbasis.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.schedule.lhbasis.entity.SysDict;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author kenx
 * @since 2021-11-19
 */
@Mapper
public interface SysDictMapper extends BaseMapper<SysDict> {

}
