package com.schedule.lhbasis.controller;

import com.schedule.lhbasis.entity.SysLogEntity;
import com.schedule.lhbasis.framework.result.Result;
import com.schedule.lhbasis.service.ISysLogService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/basis/sysLog")
@Api("系统日志模块")
public class SysLogController {

    @Autowired
    private ISysLogService sysLogService;

    @PostMapping("list")
    // @SysLog(oneLevel = "日志操作",towLevel = "查询",requestType = OperationType.SELECT)
    public Result list(){
        int a=1/0;
        return Result.success(sysLogService.list());
    }

    @PostMapping("list2")
    // @SysLog(oneLevel = "日志操作",towLevel = "查询2",requestType = OperationType.SELECT)
    public Result list2(){
        int a=1/0;
        return Result.success(sysLogService.list());
    }

    @PostMapping("list3")
    // @SysLog(oneLevel = "日志操作",towLevel = "查询3",requestType = OperationType.SELECT)
    public Result list3(){
        return Result.success(sysLogService.list());
    }


    @PostMapping("add")
    public Result add(@RequestBody SysLogEntity sysLogEntity){
        return Result.success(sysLogService.save(sysLogEntity));
    }


    @PostMapping("del")
    public Result del(@RequestBody String id){
        return Result.success(sysLogService.removeById(id));
    }


    @PostMapping("update")
    public Result update(@RequestBody SysLogEntity sysLogEntity){
        return Result.success(sysLogService.updateById(sysLogEntity));
    }
}
