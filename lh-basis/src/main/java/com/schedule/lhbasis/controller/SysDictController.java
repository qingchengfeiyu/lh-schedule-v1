package com.schedule.lhbasis.controller;

import com.schedule.lhbasis.entity.SysDict;
import com.schedule.lhbasis.service.ISysDictService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author kenx
 * @since 2021-11-19
 */
@RestController
@RequestMapping("/basis/sysDict")
@Api("系统字典模块")
public class SysDictController {

    @Autowired
    ISysDictService iSysDictService;


    @PostMapping("add")
    public boolean add(SysDict sysDict){
        return iSysDictService.save(sysDict);
    }
}
