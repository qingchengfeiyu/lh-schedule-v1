package com.schedule.lhbasis;

import com.schedule.lhbasis.framework.result.Result;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.flyway.FlywayAutoConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@RestController
// 不用默认配置的Flyway，自己配置
@SpringBootApplication(exclude = {
        FlywayAutoConfiguration.class
})
@EnableAsync
@MapperScan("com.schedule.*.mapper")
@EnableScheduling
public class LhBasisApplication {

    private static ApplicationContext applicationContext;

    public static void main(String[] args) {
        applicationContext = SpringApplication.run(LhBasisApplication.class, args);
        displayAllBeans();
    }

    /**
     * 访问首页提示
     * @return /
     */
    @GetMapping("/")
    public Result index() {
        return Result.success( "lh-schedule-v1 started successfully");
    }

    /**
     * 打印所以装载的bean
     */
    public static void displayAllBeans() {
        String[] allBeanNames = applicationContext.getBeanDefinitionNames();
        List<String> controller = Arrays.stream(allBeanNames).filter(Str -> Str.endsWith("Controller")).collect(Collectors.toList());
        List<String> serviceImpl = Arrays.stream(allBeanNames).filter(Str -> Str.endsWith("ServiceImpl")).collect(Collectors.toList());
        List<String> mapper = Arrays.stream(allBeanNames).filter(Str -> Str.endsWith("Mapper")
                && !Str.equals("jacksonObjectMapper")
                && !Str.equals("healthHttpCodeStatusMapper")
                && !Str.equals("endpointOperationParameterMapper")
                && !Str.equals("webEndpointPathMapper")
        ).collect(Collectors.toList());

        System.out.println("\033[1;35m"+"装配的Controller如下："+controller+"\033[m");
        System.out.println("\033[1;35m"+"装配的serviceImpl如下："+serviceImpl+"\033[m");
        System.out.println("\033[1;35m"+"装配的mapper如下："+mapper+"\033[m");
    }
}
